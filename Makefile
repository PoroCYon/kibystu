# Copyright © 2017 PoroCYon
# This work is free. It comes without any warranty, to the extent
# permitted by applicable law. You can redistribute it and/or modify it
# under the terms of the Do What The Fuck You Want To Public License,
# Version 2, as published by Sam Hocevar. See http://www.wtfpl.net/ for
# more details.

DOCROOT:=/

ifneq ($(wildcard config.mk),)
include config.mk
endif

default: all

DATE:="$(shell LC_TIME=C date "+%B %e, %Y")"

MAINTITLE:="lo kibystu pe la poros"

AWK    ?= awk
PANDOC ?= pandoc
SCSSC  ?= sassc
PDMARKDOWN:=markdown_github+raw_tex+yaml_metadata_block+table_captions+implicit_figures+simple_tables+multiline_tables+grid_tables+tex_math_dollars+fenced_code_attributes+inline_code_attributes+markdown_attribute+fancy_lists+all_symbols_escapable+superscript+subscript+header_attributes-emoji-hard_line_breaks
PDFLAGS=-s --ascii -M date=$(DATE) -V docroot="$(DOCROOT)" \
    -V maintitle=$(MAINTITLE) -c "$(DOCROOT)stc/style-light.css" --mathml \
    $(SIDEBAR_FILES_CLEAN)
#    -H tpl/header.html \
#-B tpl/before.html -A tpl/after.html

# dev machine has 2.0, prod one has 1.7
ifeq ($(shell $(PANDOC) --version | head -1 | grep -Fq " 2" && echo new || echo old),new)
# '+=' automatically inserts a space >__>
	PDMARKDOWN:=$(PDMARKDOWN)+smart
else
	PDFLAGS += -S
endif

INPUT_FILES:=$(shell find src/ -type f -name "*.md")
SIDEBAR_FILES:=$(shell find src/ -mindepth 1 -maxdepth 1 -type f -name "*.md" \
    -and -not -name "index.md" -and -not -name "ictsblock.md" | sort)
SIDEBAR_FILES_CLEAN=$(patsubst src/%.md,-V sidebar-links=%,$(SIDEBAR_FILES))
OUTPUT_FILES:=$(patsubst src/%.md, out/%.html, $(INPUT_FILES))

DATERFC3339:=$(shell LC_TIME='C' date -u '+%FT%TZ')
DATERFC822:=$(shell  LC_TIME='C' date -u '+%c')

TPL_SHARED=tpl/after.html tpl/before.html #tpl/header.html
STY_SHARED=sty/style-common.scss sty/solarized-base.scss

%/:
	mkdir -p "$@"

obj/postlist.html: src/blog/postdb.tsv obj/
	tac "$<" | $(AWK) -v docroot="$(DOCROOT)" -F '\t' -f src/blog/mkpostlist.awk > "$@"

obj/%.xml.pre: tpl/%.xml.pre obj/
	< "$<" sed -E -e 's/daterfc3339/$(DATERFC3339)/g' -e 's/daterfc822/$(DATERFC822) +0000/g' > "$@"
obj/%.list.xml: src/blog/mk%list.awk src/blog/postdb.tsv obj/
	tac "src/blog/postdb.tsv" | $(AWK) -F '\t' -f "$<" > "$@"
out/feed/%.xml: obj/%.xml.pre obj/%.list.xml tpl/%.xml.post
	@if ! [ -d "$(dir $@)" ]; then	mkdir -p "$(dir $@)"; fi
	cat $^ > "$@"

# the input file is first used as template, so variables in the file are
# replaced by the preprocessor as well!
out/blog.html: src/blog.md obj/postlist.html $(TPL_SHARED) tpl/template-blog.html
	$(PANDOC) $(PDFLAGS) -f html -t plain --template="$<" /dev/null | \
        $(PANDOC) $(PDFLAGS) -f $(PDMARKDOWN) --template=tpl/template-blog.html -A obj/postlist.html -t html -o "$@"

out/%.html: src/%.md $(TPL_SHARED) tpl/template.html
	@if ! [ -d "$(dir $@)" ]; then	mkdir -p "$(dir $@)"; fi
	$(PANDOC) $(PDFLAGS) -f html -t plain --template="$<" /dev/null | \
        $(PANDOC) $(PDFLAGS) -f $(PDMARKDOWN) --template=tpl/template.html -t html -o "$@"

out/stc/style-%.css: sty/style-%.scss $(STY_SHARED) sty/solarized-%.scss out/stc/
	$(SCSSC) -t compact -M "$<" "$@"

all: obj/ out/ $(wildcard stc/*) \
        out/stc/style-dark.css out/stc/style-light.css \
        out/feed/rss.xml out/feed/atom.xml \
        $(OUTPUT_FILES)
	-gpg -a --export $(GPGKEY) > out/pcy.gpg.asc
	-ln -s -T ictsblock.html out/hvu.html
	@cp -vr stc/ out/

clean:
	@rm -rvf out/ obj/

ningau.sh:
	@echo "This is a remote update mechanism. Implementation is an exercise for the reader."
install: ningau.sh
	@./ningau.sh

test: all
	cd out/ && python3 -m http.server 8088 --bind localhost

blogpost:
	./mkblogpost.sh

.PHONY: default all clean install test blogpost

.PRECIOUS: obj/ obj/atom.list.xml obj/rss.list.xml obj/atom.xml.pre obj/rss.xml.pre out/ out/stc/

