---
title: home
...

## me at various places on the Internet

* [GitLab](https://gitlab.com/PoroCYon)
* [GitHub](https://github.com/PoroCYon)
* [Pouët](https://www.pouet.net/user.php?who=99811)
* [Reddit](https://reddit.com/user/PoroCYon)
* [TCF](https://forums.terraria.org/index.php?members/.479/)
* 'PoroCYon' on a bunch of IRC networks, and 'pcy' on smaller ones
* [PGP key]($docroot$pcy.gpg.asc)

## useful stuff hosted here

* [undertale.rawr.ws mirror]($docroot$undertale)

## other stuff that's here as well

* [I maintain a list of sites mistakenly blocked by ICTS]($docroot$ictsblock.html)
* [Readonly mirror of the Linux Sizecoding Wiki]($docroot$lsc-wiki/index.html)

## more stuff, not hosted here

Perhaps I should back up that stuff instead of just linking. I don't have the
time now, though.

* [Collection of surveillance and censorship incidents
](https://forum.fork.sh/t/collection-of-surveillance-and-censorship-incidents/367/)

