{
    cmd = "LC_TIME='C' date -u '+%FT%TZ' -d \"" $2 "\""
    cmd | getline date
    close(cmd)

    cmd = "pandoc -f markdown -t html -o - src/blog/post/" $1 ".md | tr '\n' ' '"
    cmd | getline content
    close(cmd)

    cmd = "tail src/blog/post/" $1 ".md -n +3 | head -c 256 | tr '\n' ' '"
    cmd | getline summary
    close(cmd)

    print "<entry><title>" $3 "</title>" \
          "<link rel=\"alternate\" type=\"text/html\" href=\"https://pcy.ulyssis.be/blog/post/" $1 ".html\" />" \
          "<id>urn:uuid:" $4 "</id>" \
          "<published>" date "</published>" \
          "<updated>" date "</updated>" \
          "<summary>" summary "</summary>" \
          "<content type=\"text/html\"><div xmlns=\"http://www.w3.org/1999/xhtml\">" \
          content "</div></content>" \
          "<author><name>PoroCYon</name></author></entry>"
}
