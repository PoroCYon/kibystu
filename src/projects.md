---
title: projects
...

# Projects

Here's an incomplete list of stuff I worked on.

## demoscene

### prods

* ["I'd just like to interject for a moment"](https://www.pouet.net/prod.php?which=71555):
  A 512-byte Linux framebuffer intro that totally didn't lose at Evoke
  or anything. It displays an image of rms and plays the Free Software Song.
* Some stuff that doesn't deserve to be mentioned.
* More soon^tm^

### tools, barebones

* [norjohe](https://gitlab.com/PoroCYon/norjohe): a 'linker' for static
  binaries with minimal ELF headers.
* [visygau](https://gitlab.com/PoroCYon/visygau): a bunch of programs and
  scripts made to make developing stuff for framebuffer devices a little
  easier
* [zmk](https://gitlab.com/PoroCYon/zmimakfa): run a program as soon as
  a file is saved to. Useful for rapid prototyping when lacking a repl.
* [zmicirvei](https://gitlab.com/PoroCYon/zmicirvei): a script that creates
  a git commit as soon as a file in the repo is modified. Useful when
  partycoding (and other stressful situations).

## reversing & modding

* [Altar.NET](https://gitlab.com/PoroCYon/Altar.NET/): GameMaker: Studio
  file unpacker, bytecode disassembler and decompiler. Probably the most
  'feature'-complete unpacker I know of. File format docs are hosted
  [elsewhere on this website](/undertale).
* [mntotex](https://gitlab.com/PoroCYon/mntotex): A program that converts
  MuPAD notebooks to LaTeX documents. (Using MATLAB is obligatory at KU
  Leuven, and I think this policy is unacceptable, so I wrote this
  converter.)
* [unpack-xnb](https://gitlab.com/PoroCYon/unpack-xnb): An `.xnb` file
  unpacker that doesn't require XNA/FNA (or .NET in general) to be installed.
  Fails to parse compressed files, though, for some unknown reason.
* [Prism](https://github.com/TerrariaPrismTeam/Prism) [dead]: A modloader
  for Terraria that attempted to have a less hacky API than its predecessors.
  Uses MSIL injection and other cool stuff I can talk way too long about.
  It failed due to internal disputes and the exitence of an alternative,
  tModLoader.
* tAPI, tConfig [discontinued]: modloaders for Terraria 1.2 and 1.1,
  respectively. I didn't do that much work, but I was in the dev teams
  nonetheless.
* I have also made some tAPI and tConfig mods, but I don't want to list
  them here because they deserve to die in peace, in the dusty corner of
  my HDD.

## other

* [sspt](https://gitlab.com/PoroCYon/sspt/): Half a package manager. Used
  to keep track of upstream repos I use, to pull and compile the sources
  automatically when a new 'version' is available.
* [kibystu](https://gitlab.com/PoroCYon/kibystu/): This website, and a
  --- very tiny --- static site generator.
* [dotfiles](https://gitlab.com/PoroCYon/dotfiles/): My dotfiles.
* [PokeApi.NET](https://gitlab.com/PoroCYon/PokeApi.NET/): A .NET library
  for [Pokéapi](https://pokeapi.co/), made very long ago. Somehow, people
  still use this.

