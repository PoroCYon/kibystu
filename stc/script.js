
function cookie_get(name) {
    var value = "; " + document.cookie,
        parts = value.split("; " + name + "=");

    if (parts.length > 1) {
        return parts.pop().split(";").shift();
    }
}
function cookie_create(value) {
    var d = new Date();
    d.setTime(d.getTime() + (30 * 24 * 60 * 60 * 1000));
    document.cookie = value + "; expires=" + d.toUTCString();
}

function toggle_theme() {
    var links = document.getElementsByTagName("link");

    for (var i = 0; i < links.length; i++) {
        if (links[i].href.indexOf("/stc/style-dark.css") !== -1) {
            links[i].href = links[i].href.replace("dark", "light");
            cookie_create("pcy_theme=solarized_light");
            break;
        }
        if (links[i].href.indexOf("/stc/style-light.css") !== -1) {
            links[i].href = links[i].href.replace("light", "dark");
            cookie_create("pcy_theme=solarized_dark");
            break;
        }
    }
}

window.onload = function (_) {
    var theme = cookie_get("pcy_theme");

    if (theme === "solarized_dark") {
        toggle_theme();
    }
}

