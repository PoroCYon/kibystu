#!/usr/bin/env bash

set -e

read -erp "Filename: " FILENAME

DIR="src/blog/post/"
FILE="${DIR}${FILENAME}.md"

mkdir -p "$DIR"

ADDENTRY=0

if ! [ -f "$FILE" ]; then
    read -erp "Title: " TITLE

    cat > "$FILE" <<EOF
---
title: ${TITLE}
blog-comment-link: ${FILENAME}
blog-date: $(date '+%F')
...

# ${TITLE}



EOF
    ADDENTRY=1
fi

[ -z "$EDITOR" ] && export EDITOR="vim"  # good enough
$EDITOR "$FILE"
git add "$FILE"

if [ "$ADDENTRY" = 1 ] && [ -f "$FILE" ]; then
    echo -e "${FILENAME}\t$(date '+%F')\t${TITLE}\t$(uuidgen)" >> src/blog/postdb.tsv
fi

