## Linux Sizecoding wiki

### Useful links:

#### [Talks]($docroot$lsc-wiki/talks.html)

#### [Tools]($docroot$lsc-wiki/tools.html)

#### [Example code]($docroot$lsc-wiki/code.html)

#### [Explanations]($docroot$lsc-wiki/explain-dot-md.html)

#### Misc

* [Collection of useful code to get started](https://weeaboo.software/lsc)
* Packages installed by default: [Ubuntu 18.04(.2)](http://releases.ubuntu.com/18.04.2/ubuntu-18.04.2-desktop-amd64.manifest), [Ubuntu 18.10](http://releases.ubuntu.com/18.10/ubuntu-18.10-desktop-amd64.manifest), [Ubuntu 19.04](http://releases.ubuntu.com/19.04/ubuntu-19.04-desktop-amd64.manifest)
* [Las' SDL explanation]($docroot$lsc-wiki/party/revision/sdl.html)
* [Current discussion on doing ARM stuff]($docroot$lsc-wiki/explain/tinyelf-arm)

### Talk with us

Join `#lsc` on IRCnet! ([webchat](https://webchat.ircnet.net/?channels=lsc))
