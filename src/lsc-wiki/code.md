## Example code

### Basic GL stuff

* [liner (intro template)](https://github.com/shizmob/liner)
* [more GL init stuff](https://github.com/blackle/Linux-OpenGL-Examples/)
* [even smaller GL init if you only need a single fullscreen fragment shader,
   still works with default Ubuntu](https://github.com/blackle/Clutter-1k/)

### Basic synth setup

* [4klang](https://gitlab.com/PoroCYon/4klang-linux/tree/master/4klang)
* [Clinkster](https://gitlab.com/PoroCYon/4klang-linux/tree/master/clinkster)
* [Oidos](https://gitlab.com/PoroCYon/4klang-linux/tree/master/oidos)
* [V2](https://gitlab.com/PoroCYon/4klang-linux/tree/master/v2)
* TODO:
  * Quiver?
  * More ad-hocish 4k synths
  * 64klang2
  * WaveSabre? (I heard MacSlow is/will be working on this one?)
  * the Brain Control one? ("Tunefish"?)
  * others
* Ghostsyn and Axiom should work by default (see [tools]($docroot$lsc-wiki/tools))

### Source code of a few intros

* [International Shipping by Suricrasia Online
  ](https://bitbucket.org/blackle_mori/international-shipping)
* [Ninjadev dabbles in intros](https://github.com/aleksanb/fourkay)
* [scaleMARK by Suricrasia Online](https://bitbucket.org/blackle_mori/scalemark)
* [작은 (small) by Limp Ninja & K2 & TiTAN](https://gitlab.com/PoroCYon/linux-4k-intro-template),
  should be quite usable as an intro template.

### Other stuff

* [Tetris clone, in 2k](https://github.com/donnerbrenn/Tetris2k)
