---
title: How to make your own Internet
blog-comment-link: how-to-make-your-own-internet
blog-date: 2017-12-17
...

# How to make your own Internet

Inspired by [a discussion about Net Neutrality](https://forum.fork.sh/t/net-neutrality-has-been-repealed/66)
and [Motherbord's move to become an ISP](https://motherboard.vice.com/amp/en_us/article/j5djd7/motherboard-and-vice-are-building-a-community-internet-network-to-protect-net-neutrality),
I decided to discuss about how to make your own Internet, not necessarily
connected to "the" Internet. Note that this doesn't go into a lot
of detail, so make sure to read the documentation of the software if
you plan to use it.

Also, this post is not about Tor, I2P, and other overlay networks.

## Wired

It's probably the easiest to start off with wired connections. Hence,
might've even made such a separate network yourself, eg. during a
LAN party.

### Basic IP

Just connecting a few computers using LAN cables and a router or switch
usually works. Hence, the foundations are made to be decentralised,
right?

Ironically, it doesn't scale very well: someone will have to hand out
IP addresses (manual or via DHCP). Combine this with network peering,
and politics will be the most important factor of who can connect and
who cannot. Oops.

Secondly, when the network gets larger and larger, the routing tables
have to do the same, too. Nowdays, [ISPs use specialised routers to keep
the speed acceptably high](https://github.com/cjdelisle/cjdns/blob/master/doc/Whitepaper.md#why).

Using this method is still the best choice for small networks. Setting
it up is as easy as plugging in the cables. If you're using a switch,
you'll have to assign IP addresses manually, but when you're equipped
with a router, it will probably be running DHCP, so it's 0
configuration.

### CJDNS

There is a solution to this problem: [CJDNS](https://github.com/cjdelisle/cjdns/blob/master/doc/Whitepaper.md).
It is an alternative IPv6 "implementation", where every node is its own
ISP, as the address is derived from one's public key. Routes are built
from a DHT, and no route lookup has to be done at intermediate nodes
--- ['route labels'](https://github.com/cjdelisle/cjdns/blob/master/doc/Whitepaper.md#the-switch)
are used instead. It can be used directly on top of ethernet frames (or
wireless) or on UDP, and can also serve as a tunnel **for** IP packets.

All packets are encrypted and verified, but that doesn't mean it's
anonymised --- use [I2Pd (a.k.a. PurpleI2P)](https://github.com/PurpleI2P/i2pd),
an I2P implementation that can work on top
of CJDNS, for this. All peers are authenticated by a password mechanism,
so an attacker can't just plug in their sniffing device. Because IP
addresses are derived from public keys, it is much harder to misuse
[routing vulnerabilities like these](https://www.theregister.co.uk/2017/12/13/suspicious_bgp_event_routed_big_traffic_sites_through_russia/)
as well.

This authentication mechanism is also a bit of a curse, causing making
'initial connections' a bit of a hassle, and "dynamic" (or 'roaming',
i.e. connecting to a different node every once in a while) connections
are made practically impossible, until a way to do this securely is
created. This currently doesn't exist, but the cjdroute daemon has an
RPC API, so this could be implemented easily.

Setting it up invovles a bit more effort, though. The software has to
be installed, but there's a package available on most distros. (I don't
know of any Windows support, the GitHub page stated at one point that
"the poor souls have to be saved". I can't disagree with that.) If
you're building from source, it's probably a better idea to use the
`crashey` (beta) branch, as it contains some bugfixes.

Next, a config file has to be generated, like this:

```
cjdroute --gen-config > cjdroute.conf
```

This config file has to be edited in order to connect with other nodes.

To start it, run the following:

```
cjdroute < cjdroute.conf
```

The system package probably contains scripts for your init system of
choice to handle this, and a systemd module probably exists as well.

In order to edit something in the config, one has to stop `cjdroute`,
edit the file, and restart it. This is quite suboptimal, but there's
a tool called [`yrd`](https://github.com/kpcyrd/yrd) used to automate most of it. (I only discovered
its existence while writing this article, so I don't really know how
it works.)

## Wireless

On to wireless networks. Please keep in mind I have less experience
with these: I only looked into the routing protocols.

### OLSR

[OLSR](https://en.wikipedia.org/wiki/Optimized_Link_State_Routing_Protocol),
or the Optimized Link State Routing protocol, is used in a number
of community wireless mesh networks. Like CJDNS, it can be used to route
packets, but in this case, this is done proactively and flooding, and
all routes for all node`<->`node pairs are calculated. This causes rather
high bandwith and CPU usage, making it not scale very well.

### B.A.T.M.A.N.

[B.A.T.M.A.N.](https://www.open-mesh.org/projects/open-mesh/wiki)
(Better Approach To Mobile Ad-hoc Networking) is made as
a reaction to OLSR, and is maintained by the
[Freifunk community](https://freifunk.net/).
It intends to be suitable for fast-changing network topologies,
assymetric links etc., and peer discovery is done by selective
flooding of 'originator' messages. According to
[this paper](https://www.sciencedirect.com/science/article/pii/S089812211100589X/pdfft?md5=dd6187c7fb187884c6a0669978248c4f&pid=1-s2.0-S089812211100589X-main.pdf),
OLSR seems to perform better in terms of speed, but
B.A.T.M.A.N. has less packet loss. This, however, seems to be conducted
with a low amount of nodes. I'm curious about the scalability of both
networks.

Wireless mesh networks will have to meet an edge that connects to
a different place, or the network won't reach very far. This connection
can be done using a wire, or maybe over a radio connection, etc. It
would be interesting to combine these mesh networks with CJDNS.

Practically speaking, the routing software could be installed on
'anything' that speaks an 802.11 (or maybe Bluetooth as well). Something
like a PirateBox could be used to serve as uplink, if all other nodes
are mobile devices.

## Sneakernet

If the above isn't possible, it might be more suitable to "go wild",
and transport data over USB drives, or on radio waves, audio wires,
lasers, QR codes, or whatever you can imagine. I have done some
"research" about routing protocols for these 'harsh' conditions, and
those that keep routing information local seem to fare better.
A significant speedup can be gained when taking the periodicity of
couriers into account, but this increases the complexity of route
calculation even more.

Nonetheless, sneakernets could be useful when an apocalypse happens, but
routing will probably be done the 'oldsk00l' way, by copying data from
portable storage drives, and routes will be 'calculated' manually.

## Conclusion

So far we've seen a few methods to create your own 'Internet', i.e.
establishing a link between two network nodes that aren't immediate
neighbours in the network graph. TCP and UDP packets can be sent,
but this, however, only gets you so far. If stronger anonymity is
desired, put I2P on top. If you want a hostname index like DNS
(preferably distributed), Namecoin and IPNS come to mind. Peering with
other networks could pose other questions, if the protocols used are
different.

