{
    cmd = "LC_TIME='C' date -u '+%c' -d \"" $2 "\""
    cmd | getline date
    close(cmd)

    cmd = "tail src/blog/post/" $1 ".md -n +3 | head -c 256 | tr '\n' ' '"
    cmd | getline description
    close(cmd)

    print "<item><title>" $3 "</title>" \
        "<description>" description "</description>" \
        "<link>https://pcy.ulyssis.be/blog/post/" $1 ".html</link>" \
        "<guid isPermaLink=\"false\">" $4 "</guid>" \
        "<pubDate>" date " +0000</pubDate></item>"
}
