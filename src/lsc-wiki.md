---
title: Linux Sizecoding Wiki
...

# Linux Sizecoding Wiki

As the [original](https://linux.weeaboo.software/) seems to go down from time
to time, I've uploaded a readonly mirror [here]($docroot$lsc-wiki/home.md).

